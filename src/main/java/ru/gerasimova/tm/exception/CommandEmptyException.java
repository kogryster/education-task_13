package ru.gerasimova.tm.exception;

public class CommandEmptyException extends RuntimeException {

    public CommandEmptyException() {
        super("Error! Enter the command...");
    }

}
