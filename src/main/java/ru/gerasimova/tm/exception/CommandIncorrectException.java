package ru.gerasimova.tm.exception;

public class CommandIncorrectException extends RuntimeException {

    public CommandIncorrectException(String value) {
        super("Error! Сommand ``" + value + "``not supported...");
    }

}
