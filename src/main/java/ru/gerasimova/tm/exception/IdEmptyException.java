package ru.gerasimova.tm.exception;

public class IdEmptyException extends RuntimeException {

    public IdEmptyException() {
        super("Error! Id is empty...");
    }

}
