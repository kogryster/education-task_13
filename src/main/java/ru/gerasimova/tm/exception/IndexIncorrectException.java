package ru.gerasimova.tm.exception;

public class IndexIncorrectException extends RuntimeException {

    public IndexIncorrectException(Throwable cause) {
        super(cause);
    }

    public IndexIncorrectException(String value) {
        super("Error! This ``" + value + "`` is not number...");
    }

    public IndexIncorrectException() {
        super("Error! Index is incorrected...");
    }

}
